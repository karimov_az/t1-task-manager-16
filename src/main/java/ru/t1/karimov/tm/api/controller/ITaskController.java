package ru.t1.karimov.tm.api.controller;

import ru.t1.karimov.tm.exception.AbstractException;

public interface ITaskController {

    void createTask() throws AbstractException;

    void showTaskById() throws AbstractException;

    void showTaskByIndex() throws AbstractException;

    void showTaskByProjectId() throws AbstractException;

    void updateTaskById() throws AbstractException;

    void updateTaskByIndex() throws AbstractException;

    void showTasks() throws AbstractException;

    void removeTaskById() throws AbstractException;

    void removeTaskByIndex() throws AbstractException;

    void changeTaskStatusById() throws AbstractException;

    void changeTaskStatusByIndex() throws AbstractException;

    void startTaskById() throws AbstractException;

    void startTaskByIndex() throws AbstractException;

    void completeTaskById() throws AbstractException;

    void completeTaskByIndex() throws AbstractException;

    void clearTasks();

}
