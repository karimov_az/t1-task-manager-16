package ru.t1.karimov.tm.api.controller;

import ru.t1.karimov.tm.exception.AbstractException;

public interface IProjectController {

    void createProject() throws AbstractException;

    void removeProjectById() throws AbstractException;

    void removeProjectByIndex() throws AbstractException;

    void changeProjectStatusById() throws AbstractException;

    void changeProjectStatusByIndex() throws AbstractException;

    void startProjectById() throws AbstractException;

    void startProjectByIndex() throws AbstractException;

    void completeProjectById() throws AbstractException;

    void completeProjectByIndex() throws AbstractException;

    void clearProjects();

    void showProjectById() throws AbstractException;

    void showProjectByIndex() throws AbstractException;

    void updateProjectById() throws AbstractException;

    void updateProjectByIndex() throws AbstractException;

    void showProjects() throws AbstractException;

}
