package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project create(String name, String description) throws AbstractFieldException;

    Project create(String name) throws AbstractFieldException;

    Project add(Project project) throws AbstractEntityNotFoundException;

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    boolean existsById(String id);

    Project findOneById(String id) throws AbstractFieldException;

    Project findOneByIndex(Integer index) throws AbstractFieldException;

    void remove(Project project) throws AbstractEntityNotFoundException;

    Project removeById(String id) throws AbstractFieldException;

    Project removeByIndex(Integer index) throws AbstractFieldException;

    int getSize();

}
